/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.graphql.client.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.ResponseError;
import org.springframework.graphql.client.ClientGraphQlResponse;
import org.springframework.stereotype.Service;

/**
 *
 * @author vaganovdv
 */
@Service
@Slf4j
public class EntityResolver {

    private int index = 0;
    
    public String resolve(ClientGraphQlResponse response) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        if (response.isValid()) {
            
            
            Map<Object, Object> map = new LinkedHashMap((LinkedHashMap) response.getData());
            sb.append("Результаты выполнения запроса ["+map.size()+"] элементов\n");
            
            map.forEach( (key, value) -> {
            
                sb.append(String.format("%-20s", "["+key.toString()+"]"));
                sb.append(String.format("%s", ""+value.toString()));
                sb.append("\n");
                
            });
            

        }
        else
        {
            
            List<ResponseError> errors = response.getErrors();
            sb.append("Обнаружено ["+errors.size()+"] ошибок  выполнения запроса\n");
            index = 0;
            errors.stream().forEach( err-> {
                index ++;
                sb.append(String.format("%-7s", "["+index+"]"));
                sb.append(String.format("%-15s", "тип:"+ err.getErrorType() ));
                sb.append(String.format("%-55s", "размещение:"+ err.getLocations().toString() ));
                sb.append(String.format("%s", " Описание: "+ err.getMessage() ));
                sb.append("\n");
            });            
            
        }

        log.info(sb.toString());
        return sb.toString();

    }

    public <T> T resolveByType(ClientGraphQlResponse response) {

        return null;
    }

}
