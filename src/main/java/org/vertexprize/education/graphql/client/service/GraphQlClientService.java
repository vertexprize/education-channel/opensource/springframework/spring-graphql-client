/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.graphql.client.service;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.client.ClientGraphQlResponse;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 *
 * @author vaganovdv
 */
@Service
@Slf4j
public class GraphQlClientService {

    private final String prefix = String.format("%-25s", "[GRAPHQL-CLIENT]");
    private final HttpGraphQlClient client;

    public GraphQlClientService(HttpGraphQlClient client) {
        this.client = client;
    }

    public ClientGraphQlResponse executeHttpRequestByDocName(String docName) {

        log.info(prefix+"Выполнение graphql запроса ...");
        Mono<ClientGraphQlResponse> responceData = client.documentName(docName)
                .execute()
                .map(response -> {
                    if (response.isValid()) {
                        log.info(prefix + "Получен ответ на GrapQL запрос: " + response);
                    } else {
                        log.error(prefix +"Ошибка: ответ не содержит данных");
                        log.error(prefix+"Описание ошибки: "+response.getErrors());
                    }
                    return response;
                });
        
        if (responceData == null) {
            String error = prefix + "отсутвует результат запроса: " + docName;
            log.error(error);
        }
        
        ClientGraphQlResponse response = responceData.block();

        return response;
    }

    
    
    public ClientGraphQlResponse executeHttpRequestByDocNameAndVariables(String docName, Map<String, Object> variables) {

        log.info(prefix+"Выполнение graphql запроса ...");
        Mono<ClientGraphQlResponse> responceData = client.documentName(docName)
                .variables(variables)
                .execute()
                .map(response -> {
                    if (response.isValid()) {
                        log.info(prefix + "Получен ответ на GrapQL запрос: " + response);
                    } else {
                        log.error(prefix +"Ошибка: ответ не содержит данных");
                        log.error(prefix+"Описание ошибки: "+response.getErrors());
                    }
                    return response;
                });
        
        if (responceData == null) {
            String error = prefix + "отсутвует результат запроса: " + docName;
            log.error(error);
        }
        
        ClientGraphQlResponse response = responceData.block();

        return response;
    }
    
    
}
