package org.vertexprize.education.graphql.client;

import jakarta.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.graphql.client.ClientGraphQlResponse;
import org.vertexprize.education.graphql.client.service.EntityResolver;
import org.vertexprize.education.graphql.client.service.GraphQlClientService;

@SpringBootApplication
@Slf4j
public class GraphqlClientApplication {

    
    @Autowired
    GraphQlClientService graphQlClientService;
    
    @Autowired
    EntityResolver entityResolver;
    
    
    public static void main(String[] args) {
        SpringApplication.run(GraphqlClientApplication.class, args);
    }
    
    

    @PostConstruct
    public void start() {
        log.info("Подсчет количества студентов в базе данных ...");
        ClientGraphQlResponse all  = graphQlClientService.executeHttpRequestByDocName("getAllStudents");                       
        entityResolver.resolve(all);
        
        log.info("Добавление студента ...");
        ClientGraphQlResponse create  = graphQlClientService.executeHttpRequestByDocName("createStudent");                       
        
        
        String id = "a5e8dea5-7bf4-4682-9f23-3ba6bf8269fa";
        log.info("Удаление студента id = "+id);
        Map<String,  Object > variables = new HashMap<>();
        variables.put("id", id);        
        ClientGraphQlResponse deleteById  = graphQlClientService.executeHttpRequestByDocNameAndVariables("deleteStudentByIdVariable",variables);                       
        entityResolver.resolve(create);
        
        
        
        String studId = "3bad91bb-b310-493b-8e00-d74c8c4ffd9e";
        String disciplineID = "5";                      
        log.info("Добавление студенту id = "+studId+" дисциплины с идентифкатором: "+disciplineID);                
        variables = new HashMap<>();
        variables.put("discId", disciplineID);
        variables.put("studID", studId);       
        ClientGraphQlResponse addDiscipline  = graphQlClientService.executeHttpRequestByDocNameAndVariables("addDiscipline",variables);                       
        entityResolver.resolve(addDiscipline);
        
        
        disciplineID = "3";                      
        log.info("Удаление у студента id = "+studId+" дисциплины с идентифкатором: "+disciplineID);                
        variables = new HashMap<>();
        variables.put("discId", disciplineID);
        variables.put("studID", studId);       
        ClientGraphQlResponse removeDiscipline  = graphQlClientService.executeHttpRequestByDocNameAndVariables("removeDiscipline",variables);                               
        entityResolver.resolve(removeDiscipline);
        
        
        
    }

}
