/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.graphql.client.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

/**
 *
 * @author vaganovdv
 */
@Configuration
public class GraphqlClientConfig {

    @Bean
    public HttpClient createHttpClient() {
        HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
                .responseTimeout(Duration.ofMillis(5000))
                .doOnConnected(conn
                        -> conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
                        .addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));
        return httpClient;
    }

    @Bean
    public WebClient createWebClient() {

        WebClient webClient = WebClient.builder()
                .baseUrl("http://localhost:8787/graphql")                           
                .clientConnector(new ReactorClientHttpConnector(createHttpClient()))
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(3 * 1024 * 1024))
                .build();

        return webClient;
    }
    
    @Bean 
    public HttpGraphQlClient createHttpGraphqlClient()
    {
        HttpGraphQlClient graphQlClient = HttpGraphQlClient.create(createWebClient());
        return graphQlClient;
        
    } 
            

}
